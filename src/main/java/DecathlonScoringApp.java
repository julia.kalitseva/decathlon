import com.forliners.decathlon.service.CsvScoreReader;
import com.forliners.decathlon.service.ResultPublisher;
import com.forliners.decathlon.service.ScoreReader;
import com.forliners.decathlon.service.ScoreService;
import com.forliners.decathlon.service.XmlResultPublisher;


public class DecathlonScoringApp {

    public static void main(String[] args) {

        ScoreReader reader = new CsvScoreReader();
        var scores = reader.readScores();

        ScoreService scoreService = new ScoreService();
        var results = scoreService.calculateScores(scores);

        ResultPublisher publisher = new XmlResultPublisher();
        publisher.publishResults(results);

    }
}

