package com.forliners.decathlon.service;

import com.forliners.decathlon.model.Athlete;

import java.util.List;

public interface ScoreReader {

    List<Athlete> readScores();
}
