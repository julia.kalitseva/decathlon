package com.forliners.decathlon.service;

import com.forliners.decathlon.model.Athlete;
import com.forliners.decathlon.model.PointSystem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CsvScoreReader implements ScoreReader {

    @Override
    public List<Athlete> readScores() {
        try {
            return Files.readAllLines(Paths.get("src/main/resources/results.csv"))
                    .stream()
                    .filter(line -> !line.isEmpty())
                    .map(this::createAthlete)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return List.of();
        }
    }

    protected Athlete createAthlete(String line) {
        var values = line.split(";");
        var scores = Arrays.copyOfRange(values, 1, values.length);

        Map<PointSystem, Double> results = IntStream.range(0, PointSystem.values().length)
                .collect(LinkedHashMap::new, (m,i) -> m.put(
                        PointSystem.values()[i],
                        parseValue(scores[i])), Map::putAll);

        var athlete = new Athlete();
        athlete.setName(values[0]);
        athlete.setResults(results);
        return athlete;
    }

    private Double parseValue(String value) {
        var arr = value.split("\\.");
        if (arr.length == 3) {
            return Double.parseDouble(arr[0]) * 60 + Double.parseDouble(arr[1] + "." + arr[2]);
        }
        return Double.valueOf(value);
    }
}
