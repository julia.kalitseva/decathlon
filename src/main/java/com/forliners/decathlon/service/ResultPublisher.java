package com.forliners.decathlon.service;

import com.forliners.decathlon.model.Result;


public interface ResultPublisher {
    void publishResults(Result result);
}
