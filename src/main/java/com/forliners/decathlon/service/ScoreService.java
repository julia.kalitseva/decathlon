package com.forliners.decathlon.service;

import com.forliners.decathlon.model.Athlete;
import com.forliners.decathlon.model.PointSystem;
import com.forliners.decathlon.model.Result;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.forliners.decathlon.model.PointSystem.*;

public class ScoreService {

    private static final List<PointSystem> trackEvents = List.of(METER_100, METER_400, HURDLES_110M, METER_1500);
    private static final List<PointSystem> fieldEvents = List.of(LONG_JUMP, SHOT_PUT, HIGH_JUMP, DISCUS_THROW, POLE_VAULT, JAVELIN_THROW);


    public Result calculateScores(List<Athlete> athletes) {
        athletes.forEach(athlete -> {
            var total = calculateTrackScores(athlete.getResults()) + calculateFieldScores(athlete.getResults());
            athlete.setTotalScore(total);
        });
        var results = getResultScores(athletes);
        return new Result(results);
    }

    private List<Athlete> getResultScores(List<Athlete> athletes) {
        var results = athletes.stream()
                .sorted(Comparator.comparingInt(Athlete::getTotalScore).reversed())
                .collect(Collectors.toList());

        IntStream.range(0, results.toArray().length)
                .forEach(i -> results.get(i).setPlace(String.valueOf(i + 1)));

        results.stream()
                .collect(Collectors.groupingBy(Athlete::getTotalScore)).values().stream()
                .filter(equalScoreAthletes -> equalScoreAthletes.size() > 1)
                .forEach(this::sharePlaces);
        return results;
    }

    private void sharePlaces(List<Athlete> equalScoreAthletes) {
        var places = equalScoreAthletes.stream()
                .map(Athlete::getPlace)
                .collect(Collectors.joining("-"));
        equalScoreAthletes.forEach(athlete -> athlete.setPlace(places));
    }

    private int calculateTrackScores(Map<PointSystem, Double> scores) {
        return trackEvents.stream().map(event -> {
            double result = event.getParamA() * Math.pow(event.getParamB() - scores.get(event), event.getParamC());
            return new BigDecimal(result).setScale(2, RoundingMode.HALF_DOWN).intValue();
        }).mapToInt(Integer::intValue).sum();
    }

    private int calculateFieldScores(Map<PointSystem, Double> scores) {
        return fieldEvents.stream().map(event -> {
            var points = List.of(HIGH_JUMP, LONG_JUMP, POLE_VAULT).contains(event) ?
                    scores.get(event) * 100 : scores.get(event);

            double result = event.getParamA() * Math.pow(points - event.getParamB(), event.getParamC());
            return new BigDecimal(result).setScale(2, RoundingMode.HALF_DOWN).intValue();
        }).mapToInt(Integer::intValue).sum();
    }
}
