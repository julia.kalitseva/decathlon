package com.forliners.decathlon.service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.forliners.decathlon.model.Result;

import java.io.StringWriter;

public class XmlResultPublisher implements ResultPublisher {

    @Override
    public void publishResults(Result result) {
        try {
            var xml = createXml(result);
            System.out.println(xml);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    protected String createXml(Result result) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(Result.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(result, sw);

        return sw.toString();
    }
}
