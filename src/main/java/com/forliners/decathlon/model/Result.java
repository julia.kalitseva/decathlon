package com.forliners.decathlon.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Result {

    private List<Athlete> athletes;

    public Result() {
    }

    public Result(List<Athlete> athletes) {
        this.athletes = athletes;
    }

    @XmlElement
    public List<Athlete> getAthletes() {
        return athletes;
    }

    public void setAthletes(List<Athlete> athletes) {
        this.athletes = athletes;
    }
}
