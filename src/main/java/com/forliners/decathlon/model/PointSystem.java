package com.forliners.decathlon.model;

public enum PointSystem {

    METER_100("100 m", 25.4347, 18.0, 1.81),
    LONG_JUMP("Long Jump", 0.14354, 220.0, 1.4),
    SHOT_PUT("Shot put", 51.39, 1.5, 1.05),
    HIGH_JUMP("High jump", 0.8465, 75.0, 1.42),
    METER_400("400 m", 1.53775, 82.0, 1.81),
    HURDLES_110M("110 m hurdles", 5.74352, 28.5, 1.92),
    DISCUS_THROW("Discus throw", 12.91, 4.0, 1.1),
    POLE_VAULT("Pole vault", 0.2797, 100.0, 1.35),
    JAVELIN_THROW("Javelin throw", 10.14, 7.0, 1.08),
    METER_1500("1500 m", 0.03768, 480.0, 1.85);

    private String label;
    private Double paramA;
    private Double paramB;
    private Double paramC;

    PointSystem(String label, Double paramA, Double paramB, Double paramC) {
        this.label = label;
        this.paramA = paramA;
        this.paramB = paramB;
        this.paramC = paramC;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getParamA() {
        return paramA;
    }

    public void setParamA(Double paramA) {
        this.paramA = paramA;
    }

    public Double getParamB() {
        return paramB;
    }

    public void setParamB(Double paramB) {
        this.paramB = paramB;
    }

    public Double getParamC() {
        return paramC;
    }

    public void setParamC(Double paramC) {
        this.paramC = paramC;
    }
}
