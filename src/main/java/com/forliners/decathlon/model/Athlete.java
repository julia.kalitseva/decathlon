package com.forliners.decathlon.model;

import java.util.Map;

public class Athlete {

    private String name;
    private Integer totalScore;
    private String place;
    private Map<PointSystem, Double> results;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Map<PointSystem, Double> getResults() {
        return results;
    }

    public void setResults(Map<PointSystem, Double> results) {
        this.results = results;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
