package com.forliners.decathlon.service;

import com.forliners.decathlon.model.Athlete;
import com.forliners.decathlon.model.PointSystem;
import com.forliners.decathlon.model.Result;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.List;

import static com.forliners.decathlon.model.PointSystem.LONG_JUMP;
import static com.forliners.decathlon.model.PointSystem.METER_100;
import static org.junit.jupiter.api.Assertions.assertEquals;

class XmlResultPublisherTest {

    private Result result;
    private final XmlResultPublisher publisher = new XmlResultPublisher();

    @BeforeEach
    void setUp() {
        result = new Result();
        Athlete a1 = new Athlete();
        a1.setName("a1");
        a1.setTotalScore(6600);
        var results = new LinkedHashMap<PointSystem, Double>();
        results.put(METER_100, 12.61);
        results.put(LONG_JUMP, 5.5);
        a1.setResults(results);

        Athlete a2 = new Athlete();
        a2.setName("a2");
        a2.setTotalScore(5600);
        var results2 = new LinkedHashMap<PointSystem, Double>();
        results2.put(METER_100, 11.61);
        results2.put(LONG_JUMP, 4.5);
        a2.setResults(results2);

        result.setAthletes(List.of(a1, a2));
    }

    @Test
    void shouldCreateXML() throws Exception {
        var xml = publisher.createXml(result);

        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<result>\n" +
                "    <athletes>\n" +
                "        <name>a1</name>\n" +
                "        <results>\n" +
                "            <entry>\n" +
                "                <key>METER_100</key>\n" +
                "                <value>12.61</value>\n" +
                "            </entry>\n" +
                "            <entry>\n" +
                "                <key>LONG_JUMP</key>\n" +
                "                <value>5.5</value>\n" +
                "            </entry>\n" +
                "        </results>\n" +
                "        <totalScore>6600</totalScore>\n" +
                "    </athletes>\n" +
                "    <athletes>\n" +
                "        <name>a2</name>\n" +
                "        <results>\n" +
                "            <entry>\n" +
                "                <key>METER_100</key>\n" +
                "                <value>11.61</value>\n" +
                "            </entry>\n" +
                "            <entry>\n" +
                "                <key>LONG_JUMP</key>\n" +
                "                <value>4.5</value>\n" +
                "            </entry>\n" +
                "        </results>\n" +
                "        <totalScore>5600</totalScore>\n" +
                "    </athletes>\n" +
                "</result>\n", xml);
    }
}