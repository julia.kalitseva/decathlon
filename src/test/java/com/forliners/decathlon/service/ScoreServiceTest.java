package com.forliners.decathlon.service;

import com.forliners.decathlon.model.Athlete;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static com.forliners.decathlon.model.PointSystem.DISCUS_THROW;
import static com.forliners.decathlon.model.PointSystem.HIGH_JUMP;
import static com.forliners.decathlon.model.PointSystem.HURDLES_110M;
import static com.forliners.decathlon.model.PointSystem.JAVELIN_THROW;
import static com.forliners.decathlon.model.PointSystem.LONG_JUMP;
import static com.forliners.decathlon.model.PointSystem.METER_100;
import static com.forliners.decathlon.model.PointSystem.METER_1500;
import static com.forliners.decathlon.model.PointSystem.METER_400;
import static com.forliners.decathlon.model.PointSystem.POLE_VAULT;
import static com.forliners.decathlon.model.PointSystem.SHOT_PUT;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ScoreServiceTest {

    private Athlete athlete1, athlete2, athlete3;

    private final ScoreService service = new ScoreService();

    @BeforeEach
    void setUp() {
        athlete1 = new Athlete();
        athlete1.setName("a1");
        athlete1.setResults(Map.of(
                METER_100, 12.61,
                LONG_JUMP, 5.0,
                SHOT_PUT, 9.22,
                HIGH_JUMP, 1.5,
                METER_400, 60.39,
                HURDLES_110M, 16.43,
                DISCUS_THROW, 21.6,
                POLE_VAULT, 2.6,
                JAVELIN_THROW, 35.81,
                METER_1500, 325.72));

        athlete2 = new Athlete();
        athlete2.setName("a2");
        athlete2.setResults(Map.of(
                METER_100, 13.04,
                LONG_JUMP, 4.53,
                SHOT_PUT, 7.79,
                HIGH_JUMP, 1.55,
                METER_400, 64.72,
                HURDLES_110M, 18.74,
                DISCUS_THROW, 24.20,
                POLE_VAULT, 2.40,
                JAVELIN_THROW, 28.20,
                METER_1500, 410.76));

        athlete3 = new Athlete();
        athlete3.setName("a3");
        athlete3.setResults(Map.of(
                METER_100, 13.04,
                LONG_JUMP, 4.53,
                SHOT_PUT, 7.79,
                HIGH_JUMP, 1.55,
                METER_400, 64.72,
                HURDLES_110M, 18.74,
                DISCUS_THROW, 24.20,
                POLE_VAULT, 2.40,
                JAVELIN_THROW, 28.20,
                METER_1500, 410.76));
    }

    @Test
    void shouldCalculateResults() {
        var results = service.calculateScores(List.of(athlete1, athlete2, athlete3));

        assertEquals(3, results.getAthletes().size());
        assertEquals(4200, results.getAthletes().get(0).getTotalScore());
        assertEquals(3199, results.getAthletes().get(1).getTotalScore());
        assertEquals(3199, results.getAthletes().get(2).getTotalScore());
        assertEquals("1", results.getAthletes().get(0).getPlace());
        assertEquals("2-3", results.getAthletes().get(1).getPlace());
        assertEquals(
                results.getAthletes().get(1).getTotalScore(),
                results.getAthletes().get(2).getTotalScore());
        assertEquals(
                results.getAthletes().get(1).getPlace(),
                results.getAthletes().get(2).getPlace());
    }
}